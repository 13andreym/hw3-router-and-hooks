import PropTypes from "prop-types";
import Button from "../Button";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./ProductCard.scss";

export default function ProductCard(props) {
    const handleClickAddToCart = () => {
        props.showCartModal(props.product);
        props.AddToCart(props.product);
    };

    const handleClickAddToFavorites = () => {
        props.AddToFavorites(props.product);
    };

    const handleItemCartClose = () => {
        props.showCartModal(props.product);
    };

    const { name, price, image } = props.product;
    const { inCart, inFavorites } = props;
    return (
        <div className="product-card">
            <div className="image-wrapper">
                <img src={image} alt={name} />
            </div>
            <h3 className="product-name">{name}</h3>

            <div className="card-footer">
                {!inFavorites && (
                    <Button
                        className="add-favorite"
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{ color: "white" }}
                        />
                    </Button>
                )}
                {inFavorites && (
                    <Button
                        className="add-favorite"
                        style={{
                            backgroundColor: "#3C4242",
                            borderRadius: "60px",

                        }}
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{ color: "white" }}
                        />
                    </Button>
                )}
                <p className="product-price">{price} грн</p>
            </div>
            {!inCart && (
                <Button className="add-to-cart" onClick={handleClickAddToCart}>
                    Explore Now!
                </Button>
            )}
            {inCart && (
                <Button
                    className="remove-to-cart"
                    onClick={handleItemCartClose}
                >
                    Product in cart!
                </Button>
            )}
        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    children: PropTypes.any,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    AddToCart: PropTypes.func,
    RemoveFromCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    ItemCartClose: PropTypes.func,
};
