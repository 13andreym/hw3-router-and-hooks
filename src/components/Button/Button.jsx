import cn from "classnames";
import PropTypes from "prop-types";
import "./Button.scss";

export default function Button(props) {
    const { classNames, backgroundColor, children, click, ...restProps } =
        props;

    return (
        <button
            className={cn("btn", classNames)}
            style={{ backgroundColor }}
            onClick={click}
            {...restProps}
        >
            {children}
        </button>
    );
}

Button.defaultProps = {
    type: "button",
    click: () => {},
};

Button.propTypes = {
    classNames: PropTypes.string,
    backgroundColor: PropTypes.string,
    children: PropTypes.any,
    click: PropTypes.func,
};

