import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Route, Routes } from "react-router-dom";
import ProductList from "./components/ProductList";
import Modal from "./components/Modal";
import Button from "./components/Button/Button.jsx";
import Header from "./components/Header/Header.jsx";


import PageCart from "./pages/PageCart/index.js";
import PageFavorites from "./pages/PageFavorites/index.js";
import { sendRequest } from "./helpers/sendRequest";
import "./App.scss";

export default function App() {
    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState(
        JSON.parse(localStorage.getItem("cartItems")) || []
    );
    const [favoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem("favoriteItems")) || []
    );
    const [itemToRemove, setItemToRemove] = useState([]);

    const [showCartModal, setShowCartModal] = useState(false);
    const [showFavoritesModal, setShowFavoritesModal] = useState(false);
    const [showConfirmationModal, setShowConfirmationModal] = useState(false);

    useEffect(() => {
        sendRequest("products.json").then((products) => {
            setProducts(products);
        });
    }, []);

    const handleAddToCart = (product) => {
        const indexItem = cartItems.findIndex((item) => item.id === product.id);

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
                quantity: 1,
            };
            const mergedCartItems = [...cartItems, newItem];
            setCartItems(mergedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(mergedCartItems));
        } else {
            const updatedItem = {
                ...cartItems[indexItem],
                quantity: cartItems[indexItem].quantity + 1,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems.splice(indexItem, 1, updatedItem);

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        }
    };

    const handleRemoveFromCart = (product) => {
        const updatedCartItems = [...cartItems];

        const indexItem = updatedCartItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem !== -1) {
            updatedCartItems[indexItem].quantity -= 1;

            if (updatedCartItems[indexItem].quantity === 0) {
                updatedCartItems.splice(indexItem, 1);
            }

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        }
    };

    const handleAddToFavorites = (product) => {
        const indexItem = favoriteItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
            };
            const mergedFavoriteItems = [...favoriteItems, newItem];

            setFavoriteItems(mergedFavoriteItems);
            localStorage.setItem(
                "favoriteItems",
                JSON.stringify(mergedFavoriteItems)
            );
        } else {
            const updatedFavoriteItems = favoriteItems.filter(
                (item) => item.id !== product.id
            );
            setFavoriteItems(updatedFavoriteItems);
            localStorage.setItem(
                "favoriteItems",
                JSON.stringify(updatedFavoriteItems)
            );
        }
    };

    const handleToggleCartModal = () => {
        setShowCartModal(!showCartModal);
    };

    const handleToggleFavoritesModal = () => {
        setShowFavoritesModal(!showFavoritesModal);
    };

    const handleToggleConfirmationModal = () => {
        setShowConfirmationModal(!showConfirmationModal);
    };

    const handleOutsideCartModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleCartModal();
        }
    };

    const handleOutsideFavoritesModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleFavoritesModal();
        }
    };

    const handleOutsideConfirmationModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleConfirmationModal();
        }
    };

    const removeItemFromCart = (product) => {
        const updatedCartItems = cartItems.filter(
            (item) => item.id !== product.id
        );

        setCartItems(updatedCartItems);
        localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        console.log(updatedCartItems);
    };

    const handleItemFavoriteClose = (product) => {
        const updatedFavoriteItems = favoriteItems.filter(
            (item) => item.id !== product.id
        );

        setFavoriteItems(updatedFavoriteItems);
        localStorage.setItem(
            "favoriteItems",
            JSON.stringify(updatedFavoriteItems)
        );
    };

    const handleRemoveClick = (item) => {
        setItemToRemove(item);
        handleToggleConfirmationModal();
    };

    const handlerConfirmRemove = () => {
        if (itemToRemove) {
            removeItemFromCart(itemToRemove);
        }
        handleToggleConfirmationModal();
    };

    const cartItemTotal = cartItems.reduce(
        (total, item) => total + item.quantity,
        0
    );
    const totalPrice = cartItems.reduce(
        (total, item) => total + item.price * item.quantity,
        0
    );

    return (
        <div className="app-wrapper">
            <Header
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                cartItemTotal={cartItemTotal}
                handleToggleCartModal={handleToggleCartModal}
                handleToggleFavoritesModal={handleToggleFavoritesModal}
            />
            <Routes>
                <Route
                    path="/"
                    element={
                        <ProductList
                            products={products}
                            cartItems={cartItems}
                            favoriteItems={favoriteItems}
                            AddToCart={handleAddToCart}
                            AddToFavorites={handleAddToFavorites}
                            showCartModal={handleToggleCartModal}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <PageFavorites
                            favoriteItems={favoriteItems}
                            handleItemFavoriteClose={handleItemFavoriteClose}
                        />
                    }
                />
                <Route
                    path="/cart"
                    element={
                        <PageCart
                            cartItems={cartItems}
                            favoriteItems={favoriteItems}
                            showCartModal={handleToggleCartModal}
                            AddToFavorites={handleAddToFavorites}
                            handleRemoveClick={handleRemoveClick}
                        />
                    }
                />
               
            </Routes>

            {showCartModal && (
                <Modal
                    classNames="shoping-cart__modal"
                    cartItems={cartItems}
                    closeButton={handleToggleCartModal}
                    closeModal={handleToggleCartModal}
                    handleOutsideClick={handleOutsideCartModalClick}
                    header="Кошик"
                    text={!cartItems.length && <div>Ваш кошик порожній.</div>}
                    actions={cartItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <div className="add-remove-btn__wrapper">
                                <Button
                                    className="btn minus-from-cart"
                                    onClick={() => handleRemoveFromCart(item)}
                                >
                                    -
                                </Button>
                                <div className="quantity-product">
                                    {item.quantity}
                                </div>
                                <Button
                                    className="btn plus-to-cart"
                                    onClick={() => handleAddToCart(item)}
                                >
                                    +
                                </Button>
                                <Button
                                    className="btn close-item__modal"
                                    onClick={() => removeItemFromCart(item)}
                                >
                                    &times;
                                </Button>
                            </div>
                        </div>
                    ))}
                    totalPrice={
                        cartItems.length && (
                            <div className="total-price__modal">
                                Разом: {totalPrice} грн
                            </div>
                        )
                    }
                />
            )}
            {showFavoritesModal && (
                <Modal
                    classNames="favorites__modal"
                    favoriteItems={favoriteItems}
                    closeButton={handleToggleFavoritesModal}
                    closeModal={handleToggleFavoritesModal}
                    handleOutsideClick={handleOutsideFavoritesModalClick}
                    header="Обрані"
                    text={
                        !favoriteItems.length && (
                            <div>У вас ще немає улюблених товарів.</div>
                        )
                    }
                    actions={favoriteItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <Button
                                className="btn__favorite-modal"
                                onClick={() => {
                                    handleAddToCart(item);
                                }}
                            >
                                Додати до кошика
                            </Button>
                        </div>
                    ))}
                />
            )}

            {showConfirmationModal && (
                <Modal
                    header="Ви бажаєте видалити цей товар з кошику?"
                    closeButton={true}
                    closeModal={() => handleToggleConfirmationModal()}
                    handleOutsideClick={(event) =>
                        handleOutsideConfirmationModalClick(event)
                    }
                    text={
                        <p>
                            Може він вам все ще потрібний?
                            <br />
                            Ви впевнені, що бажаєте його видалити?
                        </p>
                    }
                    actions={
                        <div className="button-container">
                            <Button
                                classNames={"btn__delete-modal"}
                                onClick={(itemToRemove) =>
                                    handlerConfirmRemove(itemToRemove)
                                }
                            >
                                Так
                            </Button>
                            <Button
                                className="btn btn__delete-modal"
                                onClick={() => handleToggleConfirmationModal()}
                            >
                                Ні
                            </Button>
                        </div>
                    }
                />
            )}
        </div>
    );
}
